package barranquero.com.sitesapp.interfaces;

import android.view.View;

/**
 * Created by usuario on 13/02/17
 * SitesApp
 */
public interface ClickListener {
    public void onClick(View view, int position);
    public void onLongClick(View view, int position);
}