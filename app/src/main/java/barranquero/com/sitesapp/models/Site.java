package barranquero.com.sitesapp.models;

import java.io.Serializable;

/**
 * Created by usuario on 9/02/17
 * SitesApp
 */

public class Site implements Serializable {
    int id;
    String name, link, email;

    public Site() {
    }

    public Site(String email, int id, String link, String name) {
        this.email = email;
        this.id = id;
        this.link = link;
        this.name = name;
    }

    public Site(String email, String link, String name) {
        this.email = email;
        this.link = link;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name + "\n" + this.email;
    }
}
