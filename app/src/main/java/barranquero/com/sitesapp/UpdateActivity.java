package barranquero.com.sitesapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import barranquero.com.sitesapp.models.Result;
import barranquero.com.sitesapp.models.Site;
import barranquero.com.sitesapp.utilities.RestClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by usuario on 14/02/17
 * SitesApp
 */

public class UpdateActivity extends AppCompatActivity {
    public static final String URL_API = "site";
    public static final int OK = 1;
    @BindView(R.id.idSite)
    TextView idSite;
    @BindView(R.id.nameSite)
    EditText nameSite;
    @BindView(R.id.linkSite)
    EditText linkSite;
    @BindView(R.id.emailSite)
    EditText emailSite;
    @BindView(R.id.accept)
    Button accept;
    @BindView(R.id.cancel)
    Button cancel;
    //EditText nameSite, linkSite, emailSite;
//TextView idSite;
//Button accept, cancel;
    Site s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
//idSite = (TextView) findViewById(R.id.idSite);
//nameSite = (EditText) findViewById(R.id.nameSite);
//linkSite = (EditText) findViewById(R.id.linkSite);
//emailSite = (EditText) findViewById(R.id.emailSite);
//accept = (Button) findViewById(R.id.accept);
//cancel = (Button) findViewById(R.id.cancel);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n, l, e;
                n = nameSite.getText().toString();
                l = linkSite.getText().toString();
                e = emailSite.getText().toString();
                if (n.isEmpty() || l.isEmpty())
                    Toast.makeText(UpdateActivity.this, "Please, fill the name and the link", Toast.LENGTH_SHORT).show();
                else {
                    s.setName(n);
                    s.setLink(l);
                    s.setEmail(e);
                    connection(s);
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent i = getIntent();
        s = (Site) i.getSerializableExtra("site");
        idSite.setText(String.valueOf(s.getId()));
        nameSite.setText(s.getName());
        linkSite.setText(s.getLink());
        emailSite.setText(s.getEmail());
    }

    private void connection(final Site s) {
        final ProgressDialog progreso = new ProgressDialog(this);
        Gson gson = new Gson();
        RequestParams params = new RequestParams();
//params.put("site", gson.toJson(s));
        params.put("name", s.getName());
        params.put("link", s.getLink());
        params.put("email", s.getEmail());
        RestClient.put(URL_API + "/" + s.getId(), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
// Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
//Site site;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Modified site ok";
//site = gson.fromJson(String.valueOf(result.getSites()), Site.class);
                        Intent i = new Intent();
                        Bundle mBundle = new Bundle();
                        mBundle.putInt("id", s.getId());
                        mBundle.putString("name", s.getName());
                        mBundle.putString("link", s.getLink());
                        mBundle.putString("email", s.getEmail());
                        i.putExtras(mBundle);
                        setResult(OK, i);
                        finish();
                    } else
                        message = "Error modifying the site:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(UpdateActivity.this, responseString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(UpdateActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}